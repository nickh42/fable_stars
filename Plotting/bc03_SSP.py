#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 23 11:57:31 2018

@author: nh444
"""
import ezgal
import sys

if sys.version_info[0] != 3:
    raise Exception("ezgal requires python 3")

# Load Bruzual & Charlot 2003 SSP model with solar metallicity (Z=0.02)
# and a Chabrier IMF
model = ezgal.model('bc03_ssp_z_0.02_chab.model')
# Set formation redshift
zf = 3.0
# Set cosmology
h_scale = 0.6774
omega_m = 0.3089
omega_l = 0.6911
model.set_cosmology(Om=omega_m, Ol=omega_l, h=h_scale, w=-1)

# Add F606W and F850LP filters
model.add_filter('acs_f606w')
model.add_filter('acs_f850lp')

zs = [0.23, 1.0]
# get e+k corrections. First index is the redshift, second index is the filter.
ek = model.get_ekcorrects(zf, zs=zs)
# get M/L ratios. First index is the redshift, second index is the filter.
ML = model.get_rest_ml_ratios(zf, zs=zs)

print("e+k corrections:")
print("F606W z={:.2f}: {:.4f}".format(zs[0], ek[0, 0]))
print("F850LP z={:.2f}: {:.4f}".format(zs[1], ek[1, 1]))
print("M/L:")
print("F606W z={:.2f}: {:.4f}".format(zs[0], ML[0, 0]))
print("F850LP z={:.2f}: {:.4f}".format(zs[1], ML[1, 1]))
