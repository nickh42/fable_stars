#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 09:23:17 2019

@author: nh444
"""
import numpy as np
from stars.BCG_mass_growth import save_growth_histories
from stars.BCG_mass_growth import plot_growth_histories

outdir = "/home/nh444/Documents/Fable_stars/"
outdir = "/data/curie4/nh444/project1/stars/"

h_scale = 0.6774
omega_m = 0.3089

basedir = "/data/ERCblackholes1/nh444/zooms/MDRInt/"
dirnames = [
            "c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
            "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
            "c288_MDRInt", "c320_MDRInt",
            "c352_MDRInt", "c384_MDRInt",
            "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
            "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
            "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
            "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
            "c464_MDRInt", "c496_MDRInt_SUBF",
            ]

# List of tuple pairs containing directory name and number of last snapshot
inputs = [
          ("c96_MDRInt", 25),
          ("c128_MDRInt", 25),
          ("c160_MDRInt", 25),
          ("c192_MDRInt", 25), ("c224_MDRInt", 25), ("c256_MDRInt", 25),
          ("c288_MDRInt", 25), ("c320_MDRInt", 25),
          ("c352_MDRInt", 25), ("c384_MDRInt", 25),
          ("c448_MDRInt", 25), ("c480_MDRInt", 25),
          ("c512_MDRInt", 25),
          ("c112_MDRInt", 99),
          ("c144_MDRInt", 99), ("c176_MDRInt", 99),
          ("c208_MDRInt", 99), ("c240_MDRInt", 99), ("c272_MDRInt", 99),
          ("c304_MDRInt", 99), ("c336_MDRInt", 99), ("c368_MDRInt", 99),
          ("c400_MDRInt", 99), ("c410_MDRInt", 99), ("c432_MDRInt", 99),
          ("c464_MDRInt", 99), ("c496_MDRInt_SUBF", 99)
          ]

filepath = "/data/curie4/nh444/project1/stars/BCG_growth_histories_1e14.hdf5"

#save_growth_histories(basedir, inputs, filepath, M200min=1e14,
#                      h_scale=h_scale)
kwargs = dict(M200lims_list=[[1e14, 5e15], [5e14, 5e15]],
              use_lookback_time=True,
              stack=True, plot_scatter=False, add_title=False,
              outdir=outdir, h_scale=h_scale, omega_m=omega_m)
# ----- Cumulative mass growth within 30 kpc and twice stellar half-mass radius ----- #
# =============================================================================
# plot_growth_histories(filepath, yname='Mstar_30', out_suffix="_30kpc",
#                       assembled=True, created=True, insitu=False,
#                       cumulative=True, normalise=True, plot_obs=True, plot_sims=True, **kwargs)
# #plot_growth_histories(filepath, yname='Mstar_50', out_suffix="_50kpc",
# #                      assembled=True, created=True, insitu=False,
# #                      cumulative=True, normalise=True, plot_obs=True, plot_sims=True, **kwargs)
# #plot_growth_histories(filepath, yname='Mstar_100', out_suffix="_100kpc",
# #                      assembled=True, created=True, insitu=False,
# #                      cumulative=True, normalise=True, plot_obs=True, plot_sims=True, **kwargs)
# =============================================================================
# =============================================================================
# plot_growth_histories(filepath, yname='StellarMassInRad', out_suffix="_halfmassrad",
#                       assembled=True, created=True, insitu=False,
#                       cumulative=True, normalise=True,
#                       plot_obs=True, plot_sims=True, show_labels=False, **kwargs)
# =============================================================================

## Halo mass versus redshift
#plot_growth_histories(filepath, yname='M200', ylims=[1e12, 5e15],
#                      assembled=True, created=True, insitu=False,
#                      cumulative=False, normalise=False,
#                      plot_obs=True, **kwargs)

# =============================================================================
# # SFR vs redshift
# plot_growth_histories(filepath, yname='SFR_50', out_suffix="_SFR_50kpc",
#                       cumulative=False, normalise=False,
#                       ylims=[0.07, 3000], leg_len=4,
#                       mass_selected=True, redshifts=np.arange(0.2, 2.1, 0.2),
#                       get_BCG_props_args=dict(basedir="/data/curie4/nh444/project1/stars/MDRInt/", dirnames=dirnames),
#                       get_BCG_props_kwargs=dict(outsuffix="_SFR", h_scale=h_scale),
#                       plot_obs=True, **kwargs)
# =============================================================================
# =============================================================================
# # sSFR vs redshift
# plot_growth_histories(filepath, yname='sSFR_50', out_suffix="_sSFR_50kpc",
#                       cumulative=False, normalise=False, leg_len=4,
#                       mass_selected=True, redshifts=np.arange(0.2, 2.1, 0.2),
#                       get_BCG_props_args=dict(basedir="/data/curie4/nh444/project1/stars/MDRInt/", dirnames=dirnames),
#                       get_BCG_props_kwargs=dict(outsuffix="_SFR", h_scale=h_scale),
#                       plot_obs=True, plot_sims=True, **kwargs)
# =============================================================================

# =============================================================================
# # ----- In-situ star formation ----- #
# kwargs['M200lims_list'] = [[1e14, 5e15]]
# ## Mass gain per redshift bin
# #plot_growth_histories(filepath, yname='Mstar_50_NSE', ylims=[0, 5e11],
# #                      M200lims_list=[[1e14, 5e15]],
# #                      assembled=True, insitu=True,
# #                      cumulative=False, use_lookback_time=True,
# #                      stack=True, plot_scatter=False, normalise=False,
# #                      outdir=outdir, h_scale=h_scale,
# #                      out_suffix="_insitu_50_diff")
# # Cumulative mass gain
# plot_growth_histories(filepath, yname='Mstar_30_NSE', out_suffix="_30kpc_NSE",
#                       ylims=[0, 1.1],
#                       assembled=True, created=True, insitu=True,
#                       cumulative=True, normalise=True, **kwargs)
# =============================================================================
