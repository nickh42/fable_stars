#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 11:38:08 2018

@author: nh444
"""
import ICM.ICM as icm
import numpy as np

h_scale = 0.6774

outdir = "/home/nh444/Documents/Fable_stars/"
#outdir = "/data/curie4/nh444/project1/L-T_relations/"

indir = "/data/curie4/nh444/project1/ICM/"
simdirs = ["L40_512_MDRIntRadioEff"]
labels = ["FABLE"]

zoomdirs = ["MDRInt/"]  # corresponding to each simdir
zooms = [[
          "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
          "c224_MDRInt", "c256_MDRInt", "c288_MDRInt",
          "c320_MDRInt",
          "c352_MDRInt", "c384_MDRInt",
          "c448_MDRInt", "c480_MDRInt",
          "c512_MDRInt",
          "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
          "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
          "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
          "c464_MDRInt",
          "c496_MDRInt_SUBF",
          ]]

# Default colours.
col = ["#c7673e", "#78a44f", "#6b87c8",
       "#934fce", "#e6ab02", "#be558f", "#47af8d"]
# dark blue, light blue, orange, dark red.
col = ['#24439b', '#50BFD7', '#ED7600', '#B94400'] + col
# blue and greens
col = ['#24439b', '#068587', '#4FB99F', '#F2B134'] + col
# dark blue, green, pink
col=['#24439b', '#1b9e77', '#CE6BAF']
hlightcol = '#8292BF'
ms = 7
mew = 2
errlw = 1.7

# =============================================================================
# # Mgas fraction vs M500
# icm.plot_Mgas_vs_mass(indir, simdirs, zoomdirs, zooms, 0.0, labels=labels,
#                       M500lims=[7e12, 3e15],
#                       fractional=True, ylim=[0.01, 0.7],
#                       plot_label=False, mass_bias_arrow=True,
#                       leg_kwargs={'ncol': 2, 'borderaxespad': 0.5, 'columnspacing': 0.5,
#                                   'fontsize': 16, 'labelspacing': 0.35},
#                       do_fit=False, M500min_fit=2.9e14,
#                       col=col, ms=ms, mew=mew, errlw=0, hlightcol=hlightcol,
#                       outdir=outdir, h_scale=h_scale, fgas=0.157,
#                       outfilename="gas_mass_vs_mass_frac.pdf")
# =============================================================================

# Mgas vs M500
#icm.plot_Mgas_vs_mass(indir, simdirs, zoomdirs, zooms, 0.0, labels=labels,
#                      M500lims=[7e12, 3e15], fgas=0.157,
#                      fractional=False, ylim=[3e11, 5e14],
#                      plot_label=False, leg_len=5,
#                      do_fit=False, M500min_fit=2.9e14,
#                      col=col, ms=ms, mew=mew, errlw=0, hlightcol=hlightcol,
#                      outdir=outdir, h_scale=h_scale)

redshifts = np.arange(0.0, 1.5, 0.2)
icm.plot_Mgas_vs_redshift(indir, simdirs, zoomdirs, zooms, redshifts,
                          # M500lims_list=[[2.9e14, 1e16]], mass_slope=1.02,
                          M500lims_list=[[2.9e14, 4e15],
                                         [1e14, 3e14],
                                         [5e13, 1e14]],
                          do_fit=True, scat_idx=[0], labels=[""], leg_len=3,
                          mass_bias_arrow=True,
                          col=col, outdir=outdir,
                          ms=9, mew=mew, errlw=errlw, hlightcol=hlightcol,
                          h_scale=h_scale)
