#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 15:01:26 2018
Plot the stellar mass density profiles of BCGs compared to Stott+11
@author: nh444
"""
h_scale = 0.6774
outdir = "/home/nh444/Documents/Fable_stars/"
outdir = "/data/curie4/nh444/project1/stars/"

indir = "/data/curie4/nh444/project1/stars/"

basedir = "/data/curie4/nh444/project1/boxes/"
simdirs = ["L40_512_MDRIntRadioEff"]
labels = ["fiducial"]

zoombasedir = "/home/nh444/data/project1/zoom_ins/"
zoomdirs = ["MDRInt/"]  # corresponding to each simdir
zooms = [["c96_MDRInt", "c128_MDRInt", "c160_MDRInt",
          "c192_MDRInt", "c224_MDRInt", "c256_MDRInt",
          "c288_MDRInt", "c320_MDRInt",
          "c352_MDRInt", "c384_MDRInt",
          "c448_MDRInt", "c480_MDRInt", "c512_MDRInt",
          "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
          "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
          "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
          "c464_MDRInt",
          "c496_MDRInt_SUBF",
          ]]
#snapnums = range(4,26)
#snapnums = range(100)

import stars.stellar_mass_profiles as smp 

# Kravtsov+ 2018 comparison
z = 0.0
smp.plot_mass_profile_comparison(indir, simdirs, zoomdirs, zooms, z,
                                 nbins=50, rmax=1000.0, rsoft=2.8125,
                                 BCG_only=True, M500min=8e13, M500max=1e15,
#                                 plot_label="$z = 0$",
#                                 plot_label_pos=(0.9, 0.7),
                                 Stott=False, kravtsov=True, for_paper=True,
                                 stacked=False, projected=False, scaled=False,
                                 #density=True, cumulative=False,
                                 density=False, cumulative=True, normalise=False,
                                 outdir=outdir,
                                 outname="BCG_prof_kravtsov.pdf",
                                 labels=labels)

# =============================================================================
# # Comparison with Stott+11 high and low-z samples
# # z = 1
# smp.plot_mass_profile_comparison(indir, simdirs, zoomdirs, zooms, 1.0,
#                                  nbins=50, stacked=True, BCG_only=True,
#                                  # fit_sersic=True, #n_fixed=4.0,
#                                  # fit_radii=[2.8125/h_scale, 100.0],
#                                  M200min=2.8e14, rmax=1000.0, rsoft=2.8125,
#                                  plot_label="$z = 1.0$",
#                                  plot_label_pos=(0.9, 0.6),
#                                  density=True, cumulative=False,
#                                  #density=False, cumulative=True,
#                                  projected=True, for_paper=True,
#                                  scaled=False, outdir=outdir,
#                                  outname="BCG_prof_Stott_highz.pdf",
#                                  h_scale=h_scale, labels=labels)
# # z = 0.23
# smp.plot_mass_profile_comparison(indir, simdirs, zoomdirs, zooms, 0.2,
#                                  nbins=50, stacked=True, BCG_only=True,
#                                  # fit_sersic=True, #n_fixed=4.0,
#                                  # fit_radii=[2.8125/h_scale, 100.0],
#                                  M200min=4.7e14, rmax=1000.0, rsoft=2.8125,
#                                  plot_label="$z = 0.2$",
#                                  plot_label_pos=(0.9, 0.6),
#                                  density=True, cumulative=False,
#                                  #density=False, cumulative=True,
#                                  projected=True, for_paper=True,
#                                  scaled=False, outdir=outdir,
#                                  outname="BCG_prof_Stott_lowz.pdf",
#                                  h_scale=h_scale, labels=labels)
# =============================================================================

#==============================================================================
# indir = "/data/curie4/nh444/project1/stars/MDRInt/"
# simdir = "c448_MDRInt"#"c410_MDRInt", "c448_MDRInt"
# redshifts = [0.2, 1.0, 2.0]
# col = ['#24439b', '#ED7600', '#50BFD7', '#B94400']
# smp.plot_mass_profile_redshift(indir, simdir, redshifts, nbins=50, rmax=300.0,
#                              N=1,#M500min=2.5e14,
#                              col=col,
#                              #outname="stellar_prof_redshift_lowmass.pdf",
#                              outname="stellar_prof_redshift_highmass.pdf",
#                              outdir=outdir,
#                              BCG_only=True, projected=True, scaled=False, density=True,
#                              h_scale=h_scale, for_paper=True)
# 
#==============================================================================
