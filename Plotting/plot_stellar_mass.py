#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 20 17:56:11 2018

@author: nh444
"""
import numpy as np
import stars.plot_stellar_mass as psm

outdir = "/home/nh444/Documents/Fable_stars/"
#outdir = "/data/curie4/nh444/project1/stars/"

# where stellar masses have been saved.
datadir = "/data/curie4/nh444/project1/stars/"
basedir = "/data/curie4/nh444/project1/boxes/"  # directory of simdirs
simdirs = ["L40_512_MDRIntRadioEff/"]
labels = ["FABLE"]
zoombasedir = "/home/nh444/data/project1/zoom_ins/"
zoomdirs = ["MDRInt/"]
zooms = [[
        "c96_MDRInt", "c128_MDRInt", "c160_MDRInt", "c192_MDRInt",
        "c224_MDRInt", "c256_MDRInt", "c288_MDRInt", "c320_MDRInt",
        "c352_MDRInt", "c384_MDRInt", "c448_MDRInt",
        "c480_MDRInt",
        "c512_MDRInt",
        "c112_MDRInt", "c144_MDRInt", "c176_MDRInt", "c208_MDRInt",
        "c240_MDRInt", "c272_MDRInt", "c304_MDRInt", "c336_MDRInt",
        "c368_MDRInt", "c400_MDRInt", "c410_MDRInt", "c432_MDRInt",
        "c464_MDRInt",
        "c496_MDRInt_SUBF",
        ]]
z = 0.0
h_scale = 0.6774

# Default colours.
col = ["#c7673e", "#78a44f", "#6b87c8",
       "#934fce", "#e6ab02", "#be558f", "#47af8d"]
# dark blue, light blue, orange, dark red.
col = ['#24439b', '#92CAD6', '#ABD6BC', '#EDB47B', '#B94400'] + col
#from palettable.cartocolors.sequential import BluYl_5_r as pallette
#from palettable.cmocean.sequential import Deep_7_r as pallette
#from palettable.colorbrewer.sequential import YlGnBu_5_r as pallette
#col = pallette.mpl_colors
ms = 9
mew = 2
errlw = 1.7
lw = 5
hlightcol = '#8292BF'

outsuffix = "" # "_0888r500"  # "_SFR"

# Calculate stellar masses.
#snapnums = range(39, 100, 4)[::-1]
#snapnums = range(25, 26)[::-1]
#snapnums = [99]
#from stars.stellar_mass import stellar_mass
#for simdir in simdirs:
#    mstar = stellar_mass(basedir, simdir, datadir, snapnums,
#                         "/data/vault/nh444/ObsData/", delta=500.0, ref="crit",
#                         h_scale=h_scale, use_fof=True, masstab=False,
#                         mpc_units=False)
#    mstar.get_stellar_masses(M500min=10**12.6, outsuffix=outsuffix, get_SFR=True,
#                             get_BCG_descendant=False, verbose=True)
#for i, zoomdir in enumerate(zoomdirs):
#    if zoomdir is not "":
#        for zoom in zooms[i]:
#            mstar = stellar_mass(zoombasedir+zoomdir, zoom+"/",
#                                 "/data/curie4/nh444/project1/stars/"+zoomdir,
#                                 snapnums,
#                                 "/data/vault/nh444/ObsData/",
#                                 delta=500.0, ref="crit", h_scale=h_scale,
#                                 use_fof=True, masstab=False,
#                                 mpc_units=True)
#            mstar.get_stellar_masses(M500min=10**12.6, RminLowRes=5.0,
#                                     outsuffix=outsuffix, get_SFR=True,
#                                     get_BCG_descendant=False, verbose=True)

# Total stellar mass vs halo mass
#psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, 0.0, toPlot="total",
#                      outfilename="stellar_mass_vs_halo_mass.pdf",
#                      incl_ICL=True, insuffix=outsuffix,
#                      projected=False, verbose=True, outdir=outdir,
#                      plot_sims=True, plot_obs=True, for_paper=True,
#                      fstar=0.01,
#                      do_fit=False, M500min_fit=1e14,
#                      labels=labels, leg_len=5, plot_label=False,
#                      col=col, ms=ms, mew=mew, errlw=0, lw=lw,
#                      hlightcol=hlightcol, h_scale=h_scale)
# Total stellar mass fraction vs halo mass
psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, 0.0, toPlot="total",
                      fractionOfM500=True, ylims=[0.005, 0.1],
                      outfilename="stellar_mass_vs_halo_mass_frac.pdf",
                      incl_ICL=True, insuffix=outsuffix,
                      projected=False, verbose=True, outdir=outdir,
                      plot_sims=True, plot_obs=True, for_paper=True,
                      fstar=0.01,
                      do_fit=False, M500min_fit=1e14,
                      labels=labels, plot_label=False, mass_bias_arrow=True,
                      leg_kwargs={'loc': 'upper right'},
                      col=col, ms=ms, mew=mew, errlw=0, lw=lw,
                      hlightcol=hlightcol, h_scale=h_scale)
# =============================================================================
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, 0.0,
# #                      toPlot="total", toPlot_denom="total_0888r500",
# #                      toPlot="ICL", toPlot_denom="ICL_0888r500",
#                       toPlot="sats", toPlot_denom="sats_0888r500",
#                       BCG_radii=[30],
#                       ylims=[0.95, 1.3], plot_med=True,
#                       outfilename="stellar_mass_ratio.pdf",
#                       incl_ICL=True, insuffix=outsuffix,
#                       projected=False, verbose=True, outdir=outdir,
#                       plot_sims=False, plot_obs=False, for_paper=True,
#                       labels=labels, plot_label=False,
#                       leg_kwargs={'loc': 'upper right'},
#                       col=col, ms=ms, mew=mew, errlw=0, lw=lw,
#                       hlightcol=hlightcol, h_scale=h_scale)
# =============================================================================
# =============================================================================
# # Total stellar mass vs redshift
# redshifts = np.arange(0, 1.5, 0.2)
# psm.plot_mstar_v_redshift(datadir, simdirs, zoomdirs, zooms, redshifts,
#                           do_fit=True,
#                           mass_slope=None,
#                           M500lims_list=[[2.9e14, 4e15],
#                                          [1e14, 2.9e14],
#                                          [5e13, 1e14],
#                                          ],
#                           # M500lims_list=[[2.9e14, 1e16]], mass_slope=0.92,
#                           # M500lims_list=[[1e14, 1e16]], mass_slope=0.96,
#                           leg_len=3, mass_bias_arrow=True,
#                           outdir=outdir, scat_idx=[0], insuffix=outsuffix,
#                           for_paper=True, labels=[""], hlightcol=hlightcol,
#                           col=['#24439b', '#1b9e77', '#CE6BAF'],
#                           ms=ms, mew=mew, errlw=errlw, h_scale=h_scale)
# =============================================================================

# Fraction of stellar mass in different components
kwargs = {'projected': False, 'incl_ICL': True, 'fractionOfTotMstar': True,
          'BCG_radii': [30],
          'M500lims': [3e12, 3e15],
          'scat_idx': [0], 'plot_med': False,
          'figsize': (7, 5.5),  'insuffix': outsuffix,
          'plot_obs': True, 'plot_sims': False, 'for_paper': True,
          'labels': [""], 'col': col, 'lw': lw,
          'markers': ['o'],
          'ms': ms, 'mew': mew, 'hlightcol': hlightcol, 'outdir': outdir,
          'h_scale': h_scale, 'verbose': True}
# =============================================================================
# # BCG fraction at z=0
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, [0.0], toPlot="BCG",
#                       outfilename="BCG_frac_vs_halo_mass_z0.pdf",
#                       plot_label=False, mass_bias_arrow=True,
#                       leg_kwargs={'frameon': True, 'facecolor': 'white',
#                                   'framealpha': 1.0, 'edgecolor': 'none'},
#                       **kwargs)
# # ICL fraction at z=0
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, [0.0], toPlot="ICL",
#                       outfilename="ICL_frac_vs_halo_mass_z0.pdf",
#                       plot_label=False, mass_bias_arrow=True,
#                       leg_kwargs={'loc': 'lower right'}, **kwargs)
# # Satellite fraction at z=0
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms, [0.0], toPlot="sats",
#                       outfilename="satellite_frac_vs_halo_mass_z0.pdf",
#                       plot_label=False, mass_bias_arrow=True,
#                       leg_kwargs={'loc': 'lower right'},
#                       **kwargs)
# =============================================================================
kwargs['BCG_radii'] = [30]
kwargs['scat_idx'] = []
kwargs['plot_obs'] = False
from palettable.colorbrewer.sequential import YlGnBu_5_r as pallette
kwargs['col'] = pallette.mpl_colors
kwargs['ms'] = 5
redshifts = [0.0, 1.0, 2.0, 3.01]
# Total stellar mass vs redshift
kwargs['figsize'] = (7, 4)
kwargs['plot_med'] = True
# =============================================================================
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms,
#                       redshifts, toPlot="total", ylims=[3e10, 3.5e13],
#                       outfilename="stellar_mass_vs_halo_mass_redshift.pdf",
#                       **kwargs)
# =============================================================================
# =============================================================================
# # BCG fraction vs redshift
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms,
#                       redshifts, toPlot="BCG",
#                       outfilename="BCG_mass_vs_halo_mass_redshift.pdf",
#                       plot_label_pos=(0.07, 0.12),
#                       **kwargs)
# =============================================================================
# =============================================================================
# # ICL fraction vs redshift
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms,
#                       redshifts, toPlot="ICL",
#                       outfilename="ICL_mass_vs_halo_mass_redshift.pdf",
#                       leg_kwargs={'loc': 'upper left'}, **kwargs)
# # satellite fraction vs redshift
# psm.plot_mstar_v_mass(datadir, simdirs, zoomdirs, zooms,
#                       redshifts, toPlot="sats",
#                       outfilename="satellite_mass_vs_halo_mass_redshift.pdf",
#                       leg_kwargs={'loc': 'upper left'}, **kwargs)
# =============================================================================

# =============================================================================
# z = 0.0
# psm.plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=z,
#                      insuffix=outsuffix, projected=True,
#                      for_paper=True, scat_idx=[0], labels=labels,
#                      do_fit=False, M500min_fit=6e13,
#                      apsize=30, outfilename="BCG_mass_vs_halo_mass_30kpc.pdf",
#                      outdir=outdir, col=col, hlightcol=hlightcol,
#                      errlw=errlw, ms=ms, mew=mew,
#                      plot_med=False, mass_bias_arrow=True,
#                      h_scale=h_scale, verbose=True)
# psm.plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=z,
#                      insuffix=outsuffix, projected=True,
#                      for_paper=True, scat_idx=[0], labels=labels,
#                      do_fit=False, M500min_fit=6e13,
#                      apsize=50, outfilename="BCG_mass_vs_halo_mass_50kpc.pdf",
#                      outdir=outdir, col=col, hlightcol=hlightcol,
#                      errlw=errlw, ms=ms, mew=mew,
#                      plot_med=False, mass_bias_arrow=True,
#                      h_scale=h_scale, verbose=True)
# =============================================================================
#psm.plot_mBCG_v_mass(datadir, simdirs, zoomdirs, zooms, z=0, insuffix=outsuffix,
#                 projected=True, for_paper=True, scat_idx=[0], labels=labels,
#                 apsize=100, outfilename="BCG_mass_vs_halo_mass_100kpc.pdf",
#                 outdir=outdir, col=col, hlightcol=hlightcol,
#                 errlw=errlw, ms=ms, mew=mew,
#                 plot_med=False, h_scale=h_scale, verbose=True)

redshifts = np.arange(0, 1.5, 0.2)
#psm.plot_mBCG_v_redshift_old(datadir, simdirs, zoomdirs, zooms, redshifts,
#                             insuffix=outsuffix, labels=labels,
#                             apsize=30, projected=True,
#                             M200lim=[5e13, 1e16], scale_by_mass=True,
#                             join_points=False, for_paper=True,
#                             figsize=(14, 7), ylims=(5e10, 5e12),
#                             outfilename="BCG_mass_vs_redshift_30kpc.pdf",
#                             outdir=outdir, scat_idx=[0], med_idx=[0],
#                             med_col='#ED7600',
#                             col=col, hlightcol=hlightcol,
#                             ms=ms, mew=mew, errlw=errlw,
#                             h_scale=h_scale, verbose=False)

# =============================================================================
# psm.plot_mBCG_v_redshift_new(datadir, simdirs, zoomdirs, zooms, redshifts,
#                              insuffix=outsuffix, labels=["best-fit"],
#                              #Mpower=0.24,
#                              apsize=30, projected=True,
#                              # Mlims_list=[[1e13, 1e16], [5e13, 1e16]],
#                              Mlims_list=[[5e13, 1e16]],  # [3e14, 4e15]],
#                              ylims=(4e10, 6e12),
#                              outfilename="BCG_mass_vs_redshift_30kpc.pdf",
#                              outdir=outdir, scat_idx=[0], med_idx=[],
#                              med_col='#ED7600',
#                              col=['#24439b', '#068587', '#4FB99F', '#F2B134'],
#                              hlightcol=hlightcol,
#                              ms=ms, mew=mew, errlw=errlw,
#                              mass_bias_arrow=True,
#                              for_paper=True, h_scale=h_scale, verbose=False)
# =============================================================================
